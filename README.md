# iAlert

A android application used to send alert messages or calls to friends, family, police, hospital, fire service and etc.

# Contribution

1. Clone the repo.
2. Add features, correct bugs.
3. Send a pull request.
4. If your code rocks, your contribution will be added.

HAPPY CODING